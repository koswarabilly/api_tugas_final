<?php

    require "changePassword.php";

    function controller($code, $user, $param) {

        if($code === 'CHANGE-PASSWORD'){
            cPassword($user, $param);
        }else if($code === 'GET-RECEIPT-LIST'){
            exploreReceipt($param);
        }else if($code === 'GET-NEW-RECEIPT'){
            exploreNewReceipt($param);
        }else if($code === 'GET-POPULAR-RECEIPT'){
            explorePopularReceipt($param);
        }else if($code === 'GET-RECEIPT-DETAIL'){
            getReceiptDetail($param);
        }else if($code === 'GET-RECEIPT-STEP'){
            getReceiptStep($param);
        }else if($code === 'VIEW-RECEIPT'){
            viewReceipt($param);
        }else if($code === 'LOVE-RECEIPT'){
            loveReceipt($param);
        }else{
            $res = ['resStatus' => 'n', 'resContent' => 'request code not valid.'];
            echo json_encode($res);
        }
    }

?>