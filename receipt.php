<?php

    function exploreReceipt($param){
        global $con;
        $page = $param['page'];
        $offset = $page > 1? "OFFSET ". (($page * 10) - 10) : "";
        $sql = "select * from view_receipt order by greatest(COALESCE(input_dt,0), COALESCE(update_dt,0)) DESC limit 10 " . $offset;

        $res = $con->query($sql);
        $totalres = $con->query("select * from view_receipt");

        if($res != false){
            $fdata = selector($res);
            $tdata = selector($totalres);
            $total_page = count($tdata) == 0? 0 : (count($tdata)/10) + 1;
            $s_arr = array(
                "total_page" => (count($tdata)/10) + 1,
                "data" => $fdata
            );
            $fres = ['resStatus' => 'y', 'resContent' => $s_arr];
            echo json_encode($fres);
        }else{
            //If query is bad
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function exploreNewReceipt($param){
        global $con;
        $page = $param['page'];
        $offset = $page > 1? "OFFSET ". (($page * 10) - 10) : "";
        $sql = "select * from view_receipt order by greatest(COALESCE(input_dt,0), COALESCE(update_dt,0)) DESC limit 10 " . $offset;

        $res = $con->query($sql);
        $totalres = $con->query("select * from view_receipt");

        if($res != false){
            $fdata = selector($res);
            $tdata = selector($totalres);
            $total_page = count($tdata) == 0? 0 : (count($tdata)/10) + 1;
            $s_arr = array(
                "total_page" => $total_page,
                "data" => $fdata
            );
            $fres = ['resStatus' => 'y', 'resContent' => $s_arr];
            echo json_encode($fres);
        }else{
            //If query is bad
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function explorePopularReceipt($param){
        global $con;
        $page = $param['page'];
        $offset = $page > 1? "OFFSET ". (($page * 10) - 10) : "";
        $sql = "select * from view_receipt where viewed > 0 or loved > 0  order by greatest(COALESCE(viewed,0), COALESCE(loved,0)) DESC limit 10 " . $offset;

        $res = $con->query($sql);
        $totalres = $con->query("select * from view_receipt where viewed > 0 or loved > 0");

        if($res != false){
            $fdata = selector($res);
            $tdata = selector($totalres);
            $total_page = count($tdata) == 0? 0 : (count($tdata)/10) + 1;
            $s_arr = array(
                "total_page" => $total_page,
                "data" => $fdata
            );
            $fres = ['resStatus' => 'y', 'resContent' => $s_arr];
            echo json_encode($fres);
        }else{
            //If query is bad
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function exploreMyReceipt($param, $user){
        global $con;
        $username = $user['username'];
        $session = $user['session'];
        $page = $param['page'];
        $offset = $page > 1? "OFFSET ". (($page * 10) - 10) : "";
        $sql = "select * from view_receipt where input_by = '". $username ."' order by greatest(COALESCE(input_dt,0), COALESCE(update_dt,0)) DESC limit 10 " . $offset;

        $res = $con->query($sql);

        if($res != false){
            $fdata = selector($res);
            $total_page = count($fdata) >10 ? count($fdata)/10 + 1 : count($fdata) == 0? 0 : 1;
            $s_arr = array(
                "total_page" => $total_page,
                "data" => $fdata
            );
            $fres = ['resStatus' => 'y', 'resContent' => $s_arr];
            echo json_encode($fres);
        }else{
            //If query is bad
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function setReceipt($param, $user){
        global $con;
        $username = $user['username'];
        $session = $user['session'];
        $food_id = randomNumber(9);
        $timenow = time() * 1000;
        $sql = "insert into foceipt_db.receipt_main(food_id,food_title,food_image,food_image_type,input_by,input_dt) values ('".
                $food_id."','".
                $param['name']."','".
                $param['image']."','".
                $param['img_type'].
                "', '".$username."','".
                $timenow."');";
        $sql_detail = "insert into foceipt_db.receipt_step(food_id,step,input_by, input_dt) values('". 
                $food_id."','".
                $param['stepValue']."','".
                $username."','".
                $timenow."');";
        $sql_view = "insert into foceipt_db.receipt_detail(food_id,viewed,loved) values('".
                $food_id."',0,0);";
        $res = $con->query($sql);
        $res_detail = $con->query($sql_detail);
        $res_view = $con->query($sql_view);
        if($res != false && $res_detail != false && $res_view != false){
            history('Successfully create receipt', $username);
            $fres = ['resStatus' => 'y', 'resContent' => 'receipt created.'];
            echo json_encode($fres);
        }else{
            history('fail to create receipt', $username);
            $fres = ['resStatus' => 'n', 'resContent' => $param['stepValue']];
            echo json_encode($fres);
        }
    }

    function uptReceipt($param, $user){
        global $con;
        $username = $user['username'];
        $session = $user['session'];
        $food_id = $param['food_id'];
        $timenow = time() * 1000;
        $sql = "update foceipt_db.receipt_main set food_title = '" . 
                $param['name'] ."', food_image = '" . 
                $param['image'] . "', food_image_type = '". 
                $param['img_type'] ."', update_by = '" .
                $username. "', update_dt = '".
                $timenow. "' WHERE food_id = '". $food_id ."';";
        $sql_detail = "update foceipt_db.receipt_step set step = '". $param['stepValue'] . "', update_by = '". $username . "', update_dt = '" . $timenow ."' WHERE food_id = '" .$food_id."';";
        $res = $con->query($sql);
        $res_detail = $con->query($sql_detail);
        if($res != false && $res_detail != false){
            history('Successfully create receipt', $username);
            $fres = ['resStatus' => 'y', 'resContent' => 'receipt created.'];
            echo json_encode($fres);
        }else{
            history('fail to create receipt', $username);
            $fres = ['resStatus' => 'n', 'resContent' => $param['stepValue']];
            echo json_encode($fres);
        }
    }

    function delReceipt($param, $user){
        if(isset($user['code']) == true){
            if($user['code'] === 'wickedisgood'){
                global $con;
                $food_id = $param['food_id'];
                $timenow = time() * 1000;
                $sql = "delete from foceipt_db.receipt_main WHERE food_id = '". $food_id ."';";
                $sql_detail = "delete from foceipt_db.receipt_detail WHERE food_id = '". $food_id ."';"; 
                $sql_step = "delete from foceipt_db.receipt_step WHERE food_id = '". $food_id ."';"; 
                $res = $con->query($sql);
                $res_detail = $con->query($sql_detail);
                $res_step = $con->query($sql_step);
                if($res != false && $res_detail != false && $res_step != false){
                    history('Successfully delete receipt', 'ADMIN');
                    $fres = ['resStatus' => 'y', 'resContent' => 'receipt deleted.'];
                    echo json_encode($fres);
                }else{
                    history('fail to delete receipt', 'ADMIN');
                    $fres = ['resStatus' => 'n', 'resContent' => 'fail to delete.'];
                    echo json_encode($fres);
                }
            }else{
                $fres = ['resStatus' => 'n', 'resContent' => 'not authorized.'];
                echo json_encode($fres);
            }
        }else{
            global $con;
            $username = $user['username'];
            $session = $user['session'];
            $food_id = $param['food_id'];
            $timenow = time() * 1000;
            $sql = "delete from foceipt_db.receipt_main WHERE food_id = '". $food_id ."';";
            $sql_detail = "delete from foceipt_db.receipt_detail WHERE food_id = '". $food_id ."';"; 
            $sql_step = "delete from foceipt_db.receipt_step WHERE food_id = '". $food_id ."';"; 
            $res = $con->query($sql);
            $res_detail = $con->query($sql_detail);
            $res_step = $con->query($sql_step);
            if($res != false && $res_detail != false && $res_step != false){
                history('Successfully delete receipt', $username);
                $fres = ['resStatus' => 'y', 'resContent' => 'receipt deleted.'];
                echo json_encode($fres);
            }else{
                history('fail to delete receipt', $username);
                $fres = ['resStatus' => 'n', 'resContent' => 'fail to delete'];
                echo json_encode($fres);
            }
        }
    }

    function getReceiptDetail($param) {
        global $con;
        $sql = "select * from view_receipt where food_id = '". $param['food_id']."'" ;
        $res = $con->query($sql);
        if($res != false){
            $fdata = selector($res);
            $fres = ['resStatus' => 'y', 'resContent' => $fdata];
            echo json_encode($fres);
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function getReceiptStep($param) {
        global $con;
        $sql = "select * from receipt_step where food_id = '". $param['food_id']."'" ;
        $res = $con->query($sql);
        if($res != false){
            $fdata = selector($res);
            $fres = ['resStatus' => 'y', 'resContent' => $fdata];
            echo json_encode($fres);
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function viewReceipt($param) {
        global $con;
        $sql = "select viewed from receipt_detail where food_id = '". $param['food_id']."'" ;
        $res = $con->query($sql);
        if($res != false){
            $fdata = selector($res);
            $afres = $fdata[0]['viewed'] + 1;
            $up = "update receipt_detail set viewed = $afres where food_id = '".$param['food_id']."'";
            $res = $con->query($up);
            if($res != false){
                history('View '. $param['food_id'], 'USER');
                $fres = ['resStatus' => 'y', 'resContent' => $afres];
                echo json_encode($fres);
            }else{
                history('Fail to view '. $param['food_id'], 'USER');
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
            
        }else{
            history('Fail to view '. $param['food_id'], 'USER');
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function loveReceipt($param) {
        global $con;
        $sql = "select loved from receipt_detail where food_id = '". $param['food_id']."'" ;
        $res = $con->query($sql);
        if($res != false){
            $fdata = selector($res);
            $afres = $fdata[0]['loved'] + 0;
            $fres = ['resStatus' => 'y', 'resContent' => $afres];
            echo json_encode($fres);
            
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

    function setLoveReceipt($param, $user) {
        global $con;
        $username = $user['username'];
        $sql = "select loved from receipt_detail where food_id = '". $param['food_id']."'" ;
        $res = $con->query($sql);
        if($res != false){
            $fdata = selector($res);
            $afres = $fdata[0]['loved'] + 1;
            $up = "update receipt_detail set loved = $afres where food_id = '".$param['food_id']."'";
            $res = $con->query($up);
            if($res != false){
                history('Love on '. $param['food_id'], $username);
                $fres = ['resStatus' => 'y', 'resContent' => $afres];
                echo json_encode($fres);
            }else{
                history('Fail to love on '. $param['food_id'], $username);
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
            
        }else{
            history('Fail to love on '. $param['food_id'], $username);
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

?>