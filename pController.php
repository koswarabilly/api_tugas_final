<?php


    function pController($code, $user, $param) {

        if($code === 'SET-RECEIPT'){
            setReceipt($param, $user);
        }else if($code === 'UPT-RECEIPT'){
            uptReceipt($param, $user);
        }else if($code === 'DEL-RECEIPT'){
            delReceipt($param, $user);
        }else if($code === 'SET-RECEIPT-LOVE'){
            setLoveReceipt($param, $user);
        }else if($code === 'GET-MY-RECEIPT'){
            exploreMyReceipt($param, $user);
        }else if($code === 'GET-USER'){
            getUser($param, $user);
        }else if($code === 'GET-HISTORY'){
            getHistory($param, $user);
        }else if($code === 'GET-POST'){
            getPost($param, $user);
        }else if($code === 'EDIT-USER'){
            editUser($param, $user);
        }else if($code === 'DEL-USER'){
            delUser($param, $user);
        }else if($code === 'C-PASSWORD'){
            editUser($param, $user);
        }else{
            $res = ['resStatus' => 'n', 'resContent' => 'request code not valid.'];
            echo json_encode($res);
        }

    }
    
?>