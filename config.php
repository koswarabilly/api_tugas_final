<?php 
    session_start();

    $servername = "localhost";
    $username = "root";
    // $password = "";
    //For public computer
    $password = "123"; 
    $dbname = "foceipt_db";

    header('Content-Type: application/json');
    header("Access-Control-Allow-Origin: *");
    header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');

    $con = new mysqli($servername, $username, $password);

    $input = file_get_contents("php://input");
    if($input != null){
        $_POST = json_decode($input, true);
    }

    //Cek Koneksi
    if($con->connect_error){
        die("Connection error : ". $con->connect_error);
    }

?>