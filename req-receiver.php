<?php 
    require "config.php";
    require "checkOcreate.php";
    require "checkUser.php";
    require "checkParam.php";
    require "selector.php";
    require "inputHistory.php";
    require "controller.php";
    require "pController.php";

    //model
    require "receipt.php";
    require "random.php";

    //Check request method
    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        $res = ['resStatus' => 'n', 'resContent' => 'request method not valid.'];
        echo json_encode($res);
    }else {
        //If server request is post
        $pdata = $_POST;
        //Check if required parameter available
        if(isset($pdata['requestCode']) == 1 && $pdata['requestCode'] === "createTable"){
            $db_sql = "CREATE DATABASE IF NOT EXISTS " . $dbname;

            $con->query($db_sql);

            mysqli_select_db($con, $dbname);
            $res = checkTable();
            if($res != true) {
                //If table fail to create
                $res = ['resStatus' => 'n', 'resContent' => 'table unable to create.'];
                echo json_encode($res);
                exit();
            }else{
                $res = ['resStatus' => 'y', 'resContent' => 'table created.'];
                echo json_encode($res);
                exit();
            }
        }else if(isset($pdata['requestCode']) != 1 || isset($pdata['requestUser']) != 1 || isset($pdata['tableCreated']) != 1 || isset($pdata['requestParam']) != 1) {
            $res = ['resStatus' => 'n', 'resContent' => $pdata];
            echo json_encode($res);
        }else{
            //If server required parameter is available

            //Check if table is created
            if($pdata['tableCreated'] !== "true"){
                $db_sql = "CREATE DATABASE IF NOT EXISTS " . $dbname;

                $con->query($db_sql);

                mysqli_select_db($con, $dbname);
                $res = checkTable();
                if($res != true) {
                    //If table fail to create
                    $res = ['resStatus' => 'n', 'resContent' => 'table unable to create.'];
                    echo json_encode($res);
                    exit();
                }
            }else {
                mysqli_select_db($con, $dbname);
            }

            //Check if user is valid
            $u_object = json_decode($pdata['requestUser']);
            $user = json_decode($pdata['requestUser'], true);
            if(is_object($u_object) != 1) {
                $res = ['resStatus' => 'n', 'resContent' => 'request user not valid.'];
                echo json_encode($res);
            }else{
                
                $param = json_decode($pdata['requestParam'],true);

                //If user data is valid
                global $public_list;
                if(in_array($pdata['requestCode'], $public_list)){

                    if($pdata['requestCode'] === 'LOGIN' || $pdata['requestCode'] === 'REGISTER'){
                        loginOregister($pdata['requestCode'], $user);
                    }else if($pdata['requestCode'] === 'VERIFY') {
                        verify($user);
                    }else {
                        controller($pdata['requestCode'], $user, $param);
                    }

                }else{
                    
                    //If not login and register
                    if(validateUser($user) == false){
                        $res = ['resStatus' => 'n', 'resContent' => 'request user not authenticated.'];
                        echo json_encode($res);
                    }else{
                        $code = $pdata['requestCode'];
                        //If user is valid
                        //Check if parameter is valid
                        if(validateCode($code) == false) {
                            $res = ['resStatus' => 'n', 'resContent' => 'request code not valid.'];
                            echo json_encode($res);
                        }else{

                            //If code is valid
                            pController($pdata['requestCode'], $user, $param);
    
                        }
    
                    }
                }

                

            }

            
        }
    }
?>