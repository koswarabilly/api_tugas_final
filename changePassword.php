<?php

    function cPassword($user, $param){
        global $con;
        $sql = "";

        $username = $user['username'];
        $session = $user['session'];
        $password = $param['password'];
        $npassword = $param['new_password'];

        $sql = "select user_id, username, password, name from users where username = '$username'" ;

        $res = $con->query($sql);

        if($res != false){
            //If query is good
            $fdata = selector($res);

            //Check if password is valid
            if(password_verify($password, $fdata[0]['password'])){
                $epassword = password_hash($npassword, PASSWORD_BCRYPT, ["cost" => 10]);
                $upt = $con->query("update users set password = '$epassword' where username = '$username'");

                history('Change password successful', $fdata[0]['user_id']);
                $fres = ['resStatus' => 'y', 'resContent' => 'password changed.'];
            }else{
                history('invalid credential', $fdata[0]['user_id']);
                $fres = ['resStatus' => 'n', 'resContent' => 'invalid credential.'];
            }

            echo json_encode($fres);
        }else{

            //If query is bad
            $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
            echo json_encode($fres);
        }
    }

?>