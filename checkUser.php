<?php


    function validateUser($user){
        if(isset($user['code']) == true){
            if($user['code'] === 'wickedisgood')
            return true;
            else
            return false;
        }else{
            global $con;
            $username = $user['username'];
            $session = $user['session'];
            $sql = "select session from users where username = '$username'";
    
            if($username !== ""){
                $res = $con->query($sql);
    
                if($res != false){
                    $fdata = selector($res);
        
                    if($fdata[0]['session'] === $session){
                        return true;
                    }else{
                        return false;
                    }
        
                }else{
                    //If query is bad
                    return false;
                }
            }else{
                return false;
            }
    
        }
        
    }

    function getUser($param, $user){
        if($user['code'] === 'wickedisgood'){
            global $con;
            $sql = "select user_id, username, name from users " ;
    
            $res = $con->query($sql);
    
            if($res != false){
                $fdata = selector($res);
                $fres = ['resStatus' => 'y', 'resContent' => $fdata];
                echo json_encode($fres);
            }else{
                //If query is bad
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'not authorized.'];
            echo json_encode($fres);
        }
        
    }

    function getHistory($param, $user){
        if($user['code'] === 'wickedisgood'){
            global $con;
            $sql = "select action, input_by, input_dt from history " ;
    
            $res = $con->query($sql);
    
            if($res != false){
                $fdata = selector($res);
                $fres = ['resStatus' => 'y', 'resContent' => $fdata];
                echo json_encode($fres);
            }else{
                //If query is bad
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'not authorized.'];
            echo json_encode($fres);
        }
        
    }

    function getPost($param, $user){
        if($user['code'] === 'wickedisgood'){
            global $con;
            $sql = "select food_id, food_title, viewed, loved, input_by, name, input_dt, update_dt from view_receipt " ;
    
            $res = $con->query($sql);
    
            if($res != false){
                $fdata = selector($res);
                $fres = ['resStatus' => 'y', 'resContent' => $fdata];
                echo json_encode($fres);
            }else{
                //If query is bad
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'not authorized.'];
            echo json_encode($fres);
        }
        
    }

    function editUser($param, $user){
        if($user['code'] === 'wickedisgood'){
            global $con;
            $sql = "";
    
            $npassword = $param['new_password'];
            $user_id = $param['user_id'];
    
            $sql = "select user_id, username, password, name from users where user_id = '$user_id'" ;
    
            $res = $con->query($sql);
    
            if($res != false){
                //If query is good
                $fdata = selector($res);
    
                //Check if password is valid
                $epassword = password_hash($npassword, PASSWORD_BCRYPT, ["cost" => 10]);
                $upt = $con->query("update users set password = '$epassword' where user_id = '$user_id'");
    
                history('Change password successful', $fdata[0]['user_id']);
                $fres = ['resStatus' => 'y', 'resContent' => 'password changed.'];
    
                echo json_encode($fres);
            }else{
    
                //If query is bad
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'not authorized.'];
            echo json_encode($fres);
        }
        
    }

    function delUser($param, $user){
        if($user['code'] === 'wickedisgood'){
            global $con;
            $user_id = $param['user_id'];
            $timenow = time() * 1000;
            $sql = "delete from foceipt_db.user WHERE user_id = '". $user_id ."';";
            $res = $con->query($sql);
            if($res != false){
                history('Successfully delete user', 'ADMIN');
                $fres = ['resStatus' => 'y', 'resContent' => 'receipt created.'];
                echo json_encode($fres);
            }else{
                history('fail to delete user', 'ADMIN');
                $fres = ['resStatus' => 'n', 'resContent' => 'fail to delete'];
                echo json_encode($fres);
            }
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'not authorized.'];
            echo json_encode($fres);
        }
    }

?>