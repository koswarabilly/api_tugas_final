<?php

    function checkTable() {
        global $con;

        $listTable = [
            array('tableName' => 'receipt_main', 'tableColumn' => '
                food_id VARCHAR(18) PRIMARY KEY,
                food_title VARCHAR(30) NOT NULL,
                food_image LONGTEXT,
                food_image_type VARCHAR(10) NOT NULL,
                input_by VARCHAR(30) NOT NULL,
                input_dt VARCHAR(30) NOT NULL,
                update_by VARCHAR(30),
                update_dt VARCHAR(30)'),
            array('tableName' => 'receipt_step', 'tableColumn' => '
                food_id VARCHAR(18) PRIMARY KEY,
                step LONGTEXT NOT NULL,
                input_by VARCHAR(30) NOT NULL,
                input_dt VARCHAR(30) NOT NULL,
                update_by VARCHAR(30),
                update_dt VARCHAR(30)'),
            array('tableName' => 'users', 'tableColumn' => '
                user_id VARCHAR(18) PRIMARY KEY,
                username VARCHAR(30) NOT NULL UNIQUE KEY,
                password VARCHAR(100) NOT NULL,
                name VARCHAR(50) NOT NULL,
                session VARCHAR(10) UNIQUE KEY,
                input_by VARCHAR(30) NOT NULL,
                input_dt VARCHAR(30) NOT NULL,
                update_by VARCHAR(30),
                update_dt VARCHAR(30)'),
            array('tableName' => 'receipt_detail', 'tableColumn' => '
                food_id VARCHAR(18) PRIMARY KEY,
                viewed INT(10) NOT NULL,
                loved INT(10) NOT NULL'),
            array('tableName' => 'history', 'tableColumn' => '
                id INT(18) AUTO_INCREMENT PRIMARY KEY,
                action VARCHAR(30) NOT NULL,
                input_by VARCHAR(30) NOT NULL,
                input_dt VARCHAR(30) NOT NULL'),
        ];

        $listView = [
            array(
                'viewName' => 'view_receipt', 
                'viewColumn' => '
                    a.food_id,
                    a.food_title,
                    a.food_image,
                    a.food_image_type,
                    b.viewed,
                    b.loved,
                    a.input_by,
                    c.name,
                    a.input_dt,
                    a.update_dt',
                'viewFrom' => 'receipt_main a',
                'viewCondition' => 'receipt_detail b ON a.food_id = b.food_id LEFT JOIN users c ON a.input_by = c.username'
            ),
        ];

        $res = true;

        foreach($listTable as $x => $fres) {
            $sql = 'CREATE TABLE IF NOT EXISTS '. $fres['tableName'] . ' ( '. $fres['tableColumn'] . ' )';
            $res = $con->query($sql);
            if($res == false){
                return false;
                break;
            }
        }

        foreach($listView as $x => $fres) {
            $sql = 'CREATE OR REPLACE VIEW '. $fres['viewName'] . ' AS SELECT '. $fres['viewColumn'] . ' FROM '. $fres['viewFrom'] .' LEFT JOIN '. $fres['viewCondition'];
            $res = $con->query($sql);
            if($res == false){
                return false;
                break;
            }
        }

        if($res == true){
            return true;
        }

    }

?>