<?php


    static $public_list = [
        'LOGIN',
        'REGISTER',
        'VERIFY',
        'CHANGE-PASSWORD',
        'GET-RECEIPT-LIST',
        'GET-NEW-RECEIPT',
        'GET-POPULAR-RECEIPT',
        'GET-RECEIPT-DETAIL',
        'GET-RECEIPT-STEP',
        'VIEW-RECEIPT',
        'LOVE-RECEIPT',
        'SET-RECEIPT-VIEW',
    ];

    static $private_list = [
        'SET-RECEIPT',
        'UPT-RECEIPT',
        'DEL-RECEIPT',
        'GET-USER',
        'GET-HISTORY',
        'GET-POST',
        'EDIT-USER',
        'DEL-USER',
        'C-PASSWORD',
        'SET-RECEIPT-LOVE',
        'GET-MY-RECEIPT'
    ];


    function validateCode($code){
        global $private_list;
        if(in_array($code, $private_list)){
            return true;
        }
        return false;
    }

    function verify($user){
        global $con;
        $username = $user['username'];
        $session = $user['session'];
        $sql = "select session from users where username = '$username'";

        if($username !== ""){
            $res = $con->query($sql);

            if($res != false){
                $fdata = selector($res);
    
                if($fdata[0]['session'] === $session){
                    $fres = ['resStatus' => 'y', 'resContent' => 'valid.'];
                }else{
                    $fres = ['resStatus' => 'n', 'resContent' => 'invalid'];
                }
    
                echo json_encode($fres);
            }else{
                //If query is bad
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }
        }else{
            $fres = ['resStatus' => 'n', 'resContent' => 'user not available.'];
            echo json_encode($fres);
        }

        
    }

    function loginOregister($code, $user){
        global $con;
        if($code === 'LOGIN'){
            $username = $user['username'];
            $password = $user['password'];

            $sql = "select user_id, username, password, name from users where username = '$username'" ;

            $res = $con->query($sql);

            if($res != false && $username !== "" && $password !== ""){
                //If query is good
                $fdata = selector($res);

                //Check if password is valid
                if(password_verify($password, $fdata[0]['password'])){
                    $sessionid = randomNumber(8);
                    $upt = $con->query("update users set session = '$sessionid' where username = '$username'");
                    $s_arr = array(
                        'session' => $sessionid,
                        'username' => $username,
                        'name' => $fdata[0]['name']
                    );
                    history('Login successful', $fdata[0]['user_id']);
                    $fres = ['resStatus' => 'y', 'resContent' => $s_arr];
                }else{
                    history('Login failed on invalid credential', $fdata[0]['user_id']);
                    $fres = ['resStatus' => 'n', 'resContent' => 'invalid credential.'];
                }
    
                echo json_encode($fres);
            }else{

                //If query is bad
                $fres = ['resStatus' => 'n', 'resContent' => 'unable to get data.'];
                echo json_encode($fres);
            }

            
        }else if($code === 'REGISTER'){
            $username = $user['username'];
            $password = password_hash($user['password'], PASSWORD_BCRYPT, ["cost" => 10]);
            $name = $user['name'];

            $status = false;

            $count = 0;

            //Keep trying to add user if user_id might be used
            do {
                $user_id = randomNumber(9);
                $timenow = time() * 1000;
                $sql = "insert into foceipt_db.users(user_id,username,password,name,`session`,input_by,input_dt) values ('".
                        $user_id."','".
                        $username."','".
                        $password."','".
                        $name.
                        "', null, 'SYSTEM','".
                        $timenow."');";

                $res = $con->query($sql);
                if($res != false){
                    $status = true;
                }
                $count += 1;
            }while ($status == false && $count < 4);


            //Check if unable to create user after 3 tries
            if($status == false){
                history('Fail to create user', 'SYSTEM');
                $fres = ['resStatus' => 'n', 'resContent' => 'user unable to be created.'];
                echo json_encode($fres);
            }else{
                history('Successfully to create user', 'SYSTEM');
                $sessionid = randomNumber(8);
                $upt = $con->query("update users set session = '$sessionid' where username = '$username'");
                $s_arr = array(
                    'session' => $sessionid,
                    'username' => $username,
                    'name' => $name
                );
                $fres = ['resStatus' => 'y', 'resContent' => $s_arr];
                echo json_encode($fres);
            }
        
        }
    }

?>